# QALPL — Qui à la plus longue ?

QALPL is a web scraper to find out the longuest URL of a given domain.

## Usage
`npm start [URL] [depth] [concurrency]`

* `URL` is the URL you want to scrape;
* `depth` is how many page depth you want to scrape from the initial URL;
* `concurrency` limits the simultaneous calls.

### Example
> `npm start http://example.com/ 2 5`

## Installation
1. `git clone git@gitlab.com:epfl-dojo/qalpl.git`
1. `cd qalpl`
1. `npm i`

## Demo GIF
Watch out, this is pretty fast !
![](qalpl.gif)